package com.enuke.bouncycastle.classes;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    private Context mCtx;

    private static final String PREF_NAME_KEYS="security_keys";
    private static final String PUBLIC_KEY="public_key";
    private static final String PRIVATE_KEY="private_key";

    public SharedPrefManager(Context mCtx) {
        this.mCtx=mCtx;
    }


    public static synchronized SharedPrefManager getInstance(Context mCtx) {
        return new SharedPrefManager(mCtx);
    }

    public void saveKeys(String privateKey,String publicKey){
        SharedPreferences pref = mCtx.getSharedPreferences(PREF_NAME_KEYS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PUBLIC_KEY,publicKey);
        editor.putString(PRIVATE_KEY,privateKey);
        editor.apply();
    }

    public String[] getKeys(){
        SharedPreferences pref = mCtx.getSharedPreferences(PREF_NAME_KEYS,Context.MODE_PRIVATE);
        return new String[]{
                pref.getString(PRIVATE_KEY,"-1"),
                pref.getString(PUBLIC_KEY,"-1")
        };
    }




}
