package com.enuke.bouncycastle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.enuke.bouncycastle.classes.SharedPrefManager;
import com.enuke.bouncycastle.classes.Utils;

import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etProb, etResult;
    private Button btnEncode, btnDecode;
    private String pubKey,prKey;
    private PrivateKey k;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getKeys();
        if(prKey.equals("-1")) {generateRSA();getKeys();}


        initViews();
        setupListener();
    }

    private void getKeys() {
        String[] keys = SharedPrefManager.getInstance(getApplicationContext()).getKeys();
        prKey = keys[0];
        pubKey = keys[1];
    }

    private void setupListener() {
        btnEncode.setOnClickListener(this);
        btnDecode.setOnClickListener(this);
    }

    private void initViews() {
        etProb = findViewById(R.id.etProb);
        etResult = findViewById(R.id.etResult);
        btnDecode = findViewById(R.id.btnDecode);
        btnEncode = findViewById(R.id.btnEncode);
    }

    private void generateRSA() {
        try {
              final BigInteger RSA_EXPONENT = new BigInteger("3");
              final int RSA_CERTAINITY = 80;
              final int RSA_1024_STREIGHT = 1024;
           // Security.addProvider(new BouncyCastleProvider());

                     RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
            generator.init(new RSAKeyGenerationParameters(RSA_EXPONENT,
                    new FixedRandom(),
                    RSA_1024_STREIGHT,
                    RSA_CERTAINITY));

            AsymmetricCipherKeyPair keyPair = generator.generateKeyPair();

            RSAPrivateCrtKeyParameters privKey = (RSAPrivateCrtKeyParameters) keyPair.getPrivate();
            RSAKeyParameters pubKey = (RSAKeyParameters) keyPair.getPublic();

            RSAPublicKey pub = new RSAPublicKey(pubKey.getModulus(), pubKey.getExponent());
            RSAPrivateKey pri = new RSAPrivateKey
                    (
                            privKey.getModulus(),
                            privKey.getPublicExponent(),
                            privKey.getExponent(),
                            privKey.getP(),
                            privKey.getQ(),
                            privKey.getDP(),
                            privKey.getDQ(),
                            privKey.getQInv()
                    );

           String publicKey =  Base64.encodeToString(pub.getEncoded(),Base64.DEFAULT);

           String privateKey =  Base64.encodeToString(pri.getEncoded(),Base64.DEFAULT);

//            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
//            generator.initialize(1024,new FixedRandom());
//            KeyPair pair = generator.generateKeyPair();
//            Key pubKey = pair.getPublic();
//            final Key privKey = pair.getPrivate();
//             k = (PrivateKey) privKey;
//
//            String publicKey = new String( Base64.encode(pubKey.getEncoded(),Base64.DEFAULT));
//
//            String privateKey = new String( Base64.encode(privKey.getEncoded(),Base64.DEFAULT));
//
//            System.out.println("publicKey : " + Base64.encode(pubKey.getEncoded(),Base64.DEFAULT));
//            System.out.println("privateKey : " + Base64.encode(privKey.getEncoded(),Base64.DEFAULT));

            SharedPrefManager.getInstance(getApplicationContext()).saveKeys(privateKey, publicKey);




            }



//
//            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
//            Base64 b64 = new Base64();
//            generator.initialize(1024, new FixedRandom());
//            KeyPair pair = generator.generateKeyPair();
//            Key pubKey = pair.getPublic();
//            final Key privKey = pair.getPrivate();
//            System.out.println("publicKey : " + b64.encode(pubKey.getEncoded()));
//            System.out.println("privateKey : " + b64.encode(privKey.getEncoded()));

       catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEncode:
                //generateRSA();
                encodeData();
                break;
            case R.id.btnDecode:
                decodeData();
                break;
        }
    }

    private void encodeData() {
        if(pubKey.equals(null)||pubKey.equals("-1")){
            Toast.makeText(this, "Null Key", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(etProb.getText().toString().trim())){
            return;
        }

        try {


           byte[] result = Utils.encrypt(etProb.getText().toString().trim(),pubKey);
           etResult.setText(Base64.encodeToString(result,Base64.DEFAULT));
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void decodeData() {
        if(prKey.equals(null)||prKey.equals("-1")){
            Toast.makeText(this, "Null Key", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(etProb.getText().toString().trim())){
            return;
        }

        try {
            String result = Utils.decrypt2(etProb.getText().toString().trim(),k);
            etResult.setText(result);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
