package com.enuke.bouncycastle;

import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enuke.bouncycastle.classes.SharedPrefManager;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ActivityTwo extends AppCompatActivity implements View.OnClickListener {
    private static final String KEY_ALIAS = "myKey";
    private EditText etProb, etResult, etRC4;
    private TextView tvRC4Encrypted;
    private Button btnEncode, btnDecode;
    private String pubKey, prKey;

    private PrivateKey PRIVATE;
    private PublicKey PUBLIC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prKey = SharedPrefManager.getInstance(getApplicationContext()).getKeys()[0];
        pubKey = SharedPrefManager.getInstance(getApplicationContext()).getKeys()[0];
        getRSAKeyPair();


        initViews();
        setupListener();


    }

    private void setupListener() {
        btnEncode.setOnClickListener(this);
        btnDecode.setOnClickListener(this);
    }

    private void initViews() {
        etProb = findViewById(R.id.etProb);
        etResult = findViewById(R.id.etResult);
        btnDecode = findViewById(R.id.btnDecode);
        btnEncode = findViewById(R.id.btnEncode);
        etRC4 = findViewById(R.id.etRC4);
        tvRC4Encrypted = findViewById(R.id.tvRC4Encrypted);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEncode:
                encodeData();
                break;
            case R.id.btnDecode:
                decodeData();
                break;
        }
    }

    void encodeData() {
        if (!TextUtils.isEmpty(etProb.getText().toString().trim())) {

            if (PUBLIC != null) {
                SecretKey RC4Key = generateRC4();
                String RC4KeyString = getRC4asString(RC4Key);
                String RC4Encrypted = encryptRC4Key(RC4Key);
                etRC4.setText(RC4KeyString);
                tvRC4Encrypted.setText(RC4Encrypted);

                String result = encryptDataByRC4(RC4Key, etProb.getText().toString());
                etResult.setText(result);
            } else
                Toast.makeText(this, "No Key", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "Enter Text", Toast.LENGTH_SHORT).show();

    }

    private String getRC4asString(SecretKey RC4Key) {
        return Base64.encodeToString(RC4Key.getEncoded(), Base64.DEFAULT);
    }

    void decodeData() {
        if (!TextUtils.isEmpty(etResult.getText().toString())) {
            if (PUBLIC != null) {
                String RC4keyString = decryptRC4Key(tvRC4Encrypted.getText().toString());
                SecretKey RC4Key = getRC4fromString(RC4keyString);
                String problem = decryptDataByRC4(RC4Key,etResult.getText().toString());

                etRC4.setText(RC4keyString);
                etProb.setText(problem);

            } else
                Toast.makeText(this, "No Key", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "Enter Text", Toast.LENGTH_SHORT).show();

    }

    private KeyPair generateRSA() {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);

            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");

            generator.initialize(
                    new KeyGenParameterSpec.Builder(
                            KEY_ALIAS,
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                            .build());
            // Generates Key with given spec and saves it to the KeyStore
            return generator.generateKeyPair();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
        return null;

    }

    private SecretKey generateRC4() {
        try {
            KeyGenerator rc4KeyGenerator = KeyGenerator.getInstance("RC4");
            SecretKey key = rc4KeyGenerator.generateKey();
            System.out.println("ddd" + Base64.encodeToString(key.getEncoded(), Base64.DEFAULT));
            //  key.getEncoded();
            return key;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "RC4 Gen error", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    private SecretKey getRC4fromString(String encodedKey) {
        byte[] decodedKey = Base64.decode(encodedKey.getBytes(), Base64.DEFAULT);
        // rebuild key using SecretKeySpec
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "RC4");
    }


    KeyPair getRSAKeyPair() {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            PRIVATE = (PrivateKey) keyStore.getKey(KEY_ALIAS, null);
            PUBLIC = keyStore.getCertificate(KEY_ALIAS).getPublicKey();

            if (PRIVATE != null && PUBLIC != null) {
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                return new KeyPair(PUBLIC, PRIVATE);
            } else {
                Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
                return null;
            }
        } catch (Exception e) {

            Toast.makeText(this, "exception", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return null;
    }


    private String encryptDataByRC4(SecretKey secretKey, String inputData) {
        String encryptedData = null;
        try {
            Cipher cipher = Cipher.getInstance("RC4");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] bytes = cipher.doFinal(inputData.getBytes());
            encryptedData = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception e) {
            System.out.println(e);
            Toast.makeText(this, "enc_Error", Toast.LENGTH_SHORT).show();
        }

        return encryptedData;
    }

    private String decryptDataByRC4(SecretKey secretKey, String data) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RC4");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] encryptedData = Base64.decode(data, Base64.DEFAULT);
            byte[] decodedData = cipher.doFinal(encryptedData);
            return new String(decodedData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "error";
    }


    private String encryptRC4Key(SecretKey RC4Key) {
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, PUBLIC);
            byte[] bytes = cipher.doFinal(Base64.encode(RC4Key.getEncoded(), Base64.DEFAULT));
            return Base64.encodeToString(bytes, Base64.DEFAULT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private String decryptRC4Key(String RC4encrypted) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, PRIVATE);
            byte[] encryptedData = Base64.decode(RC4encrypted, Base64.DEFAULT);
            byte[] decodedData = cipher.doFinal(encryptedData);
            return new String(decodedData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "error";
    }


}
